<?php
//var_dump($_POST);
//die();
include_once('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

$objProfilePicture=new ProfilePicture();
if(!empty($_FILES['image']['name']))
{
    $imageName = time() . $_FILES['image']['name'];
    $temporary_location = $_FILES['image']['tmp_name'];

    move_uploaded_file($temporary_location, 'Images/' . $imageName);


    $_POST['image'] = $imageName;
}
$objProfilePicture->setData($_POST);
$objProfilePicture->update();
