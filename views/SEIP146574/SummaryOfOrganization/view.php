<?php
require_once("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrganization=new SummaryOfOrganization;

$objSummaryOfOrganization->setData($_GET);
$oneData=$objSummaryOfOrganization->view("obj");

echo "ID:".$oneData->id."<br>";
echo "Name:".$oneData->name."<br>";
echo "Organization Summary:". $oneData->organization_summary."<br>";
