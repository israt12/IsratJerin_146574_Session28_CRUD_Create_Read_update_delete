<?php
namespace App\BookTitle;

use PDO;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData=Null)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('book_title',$postVariableData))
        {
            $this->book_title=$postVariableData['book_title'];
        }
        if(array_key_exists('author_name',$postVariableData))
        {
            $this->author_name=$postVariableData['author_name'];
        }
    }
    public function store()
    {
    $arrData =array($this->book_title,$this->author_name);

    $sql="INSERT INTO book_title(book_title,author_name) VALUES (?,?)";

    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

    if($result)
        Message::message("Success!Data has been inserted successfully");
    else
        Message::message("Failed!Data has been inserted successfully");

        Utility::redirect('create.php');
    }

    //end of store
    public function index($fetchMode='ASSOC')
    {
        $sql = "SELECT * from book_title where is_deleted = 0 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    // end of index();
    public function view($fetchMode='ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public function update()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set book_title =?,author_name=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }
    public function delete()
    {
        $sql="Delete from book_title where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute();
        if($result)
            Message::message("Success!Data has been deleted successfully");
        else
            Message::message("Failed!Data has been deleted successfully");

        Utility::redirect('index.php');
    }
    public function trash()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set is_deleted=1 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success!Data has been trushed successfully");
        else
            Message::message("Failed!Data has been trushed successfully");

        Utility::redirect('index.php');

    }
    public function trashList($fetchMode='ASSOC')
    {
        $sql = "SELECT * from book_title where is_deleted = 1 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function restore()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set is_deleted=0 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::message("Success!Data has been restored successfully");
        else
            Message::message("Failed!Data has been restored successfully");

        Utility::redirect('trashList.php');
    }
}
//$objBookTitle=new BookTitle();